provider "aws" {
  region = "ap-south-1"
}

resource "aws_instance" "terr1" {
ami = "ami-0199f70948fee938a"
instance_type = "t2.micro"
subnet_id = "subnet-e3b02aaf"
iam_instance_profile = "SSM_ROLE"
user_data = <<-EOT
             #!/bin/bash
             sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
             sudo systemctl start amazon-ssm-agent
EOT
tags = {
  Name = "terr1"
}
}
resource "aws_s3_bucket" "bucketalreadyexistserrorsucks"  {
  bucket = "bucketalreadyexistserrorsucks"
  
tags = {
    Name  =  "mybucket1"
  }
}



